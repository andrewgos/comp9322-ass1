package au.edu.unsw.soacourse.utilservice;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.jws.WebService;


@WebService(endpointInterface = "au.edu.unsw.soacourse.utilservice.MarketDataUtilService")
public class MarketDataUtilServiceImpl implements MarketDataUtilService {

	ObjectFactory factory = new ObjectFactory();

	//========================================================================
	//							summariseMarketData
	//========================================================================
	@Override
	public SummariseMarketDataResponse summariseMarketData(SummariseMarketDataRequest parameters)throws SummariseMarketDataFaultMsg {
		// GET PARAMETERS
		String eventSetId = parameters.getEventSetId();
		
		// FAULT HANDLING
		String faultTestFilePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".csv";
		File faultTestFile = new File(faultTestFilePath);
		
    	if (!faultTestFile.exists()){
    		String code = "Invalid eventSetId";
    		String msg = "There's no file with the given eventSetId";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new SummariseMarketDataFaultMsg(msg, fault);
    	}
		
    	// BEGIN SERVICE
		Hashtable<String, String> hashTable = getDataFromMarketFile(eventSetId);

		// RETURN RESPONSE
		SummariseMarketDataResponse response = factory.createSummariseMarketDataResponse();
		response.setEventSetId(hashTable.get("eventSetId"));
		response.setSec(hashTable.get("sec"));
		response.setStartDate(hashTable.get("startDate"));
		response.setEndDate(hashTable.get("endDate"));
		response.setCurrencyCode(hashTable.get("currencyCode"));
		response.setFileSize(hashTable.get("fileSize"));
		return response;
	}
	
	//========================================================================
	//							convertMarketData
	//========================================================================
	@Override
	public ConvertMarketDataResponse convertMarketData(ConvertMarketDataRequest parameters)throws ConvertMarketDataFaultMsg {
		// GET PARAMETERS
		String eventSetId = parameters.getEventSetId();
		String targetCurrency = parameters.getTargetCurrency();
		String targetDate = parameters.getTargetDate();
		
		// FAULT HANDLING : CHECK IF EVENTSETID ALREADY EXISTS
		String faultTestFilePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".csv";
		File faultTestFile = new File(faultTestFilePath);
    	if (!faultTestFile.exists()){
    		String code = "Invalid eventSetId";
    		String msg = "There's no file with the given eventSetId";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketDataFaultMsg(msg, fault);
    	}
		// FAULT HANDLING : CHECK IF CURRENCY NOT FROM AUD
		Hashtable<String, String> hashTable = getDataFromMarketFile(eventSetId);
		if(!hashTable.get("currencyCode").equals("AUD")){
			String code = "Invalid content";
    		String msg = "The file already contains converted prices";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketDataFaultMsg(msg, fault);
		}
		// FAULT HANDLING : CHECK CURRENCY CODE FORMAT
		if(!targetCurrency.matches("[A-Za-z]{3}")){
			String code = "Invalid targetCurrency";
    		String msg = "Given targetCurrency is not in the format of three letter currency";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketDataFaultMsg(msg, fault);
		}
		// FAULT HANDLING : CHECK DATE FORMAT
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	try{
    		df.parse(targetDate);
    	}
    	catch(ParseException e){
    		String code = "Invalid Date format";
    		String msg = "Date must be in the format: yyyy-MM-dd";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketDataFaultMsg(msg, fault);
    	}
		// FAULT HANDLING : CHECK DAY & MONTH BOUNDARIES
    	String[] targetDateFields = targetDate.split("-");
    	int targetDateDay = Integer.parseInt(targetDateFields[2]);
    	int targetDateMonth = Integer.parseInt(targetDateFields[1]);
    	//int targetDateYear = Integer.parseInt(targetDateFields[0]);
    	// Check for invalid day
    	if (targetDateDay < 1 || targetDateDay > 31){

    		String code= "Invalid Date";
    		String msg = "Day must be between 1 and 31";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketDataFaultMsg(msg, fault);
    	}
    	// Check for invalid month
    	if (targetDateMonth < 1 || targetDateMonth > 12){
    		String code= "Invalid Month";
    		String msg = "Month must be between 1 and 12";
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		throw new ConvertMarketDataFaultMsg(msg, fault);
    	}
		
    	
		// BEGIN SERVICE
		double currencyRate = getCurrencyRate(targetCurrency, targetDate);
		String newEventSetId = UUID.randomUUID().toString();

		try {
			
			// GET MARKET DATA FILE
			String filePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".csv";
			File marketFile = new File(filePath);
			Scanner scanner = new Scanner(new FileReader(marketFile));
			
			// CHECK IF DIRECTORY EXISTS BEFORE CREATING A NEW FILE
	    	File dir = new File(System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/");
			if (!dir.exists()) 
				if(dir.mkdir())
					System.out.println("Dir created");
			
			// CREATE NEW MARKET FILE USING NEW EVENTSETID
			String newFilePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +newEventSetId +".csv";
			File newMarketFile = new File(newFilePath);
			FileWriter writer = new FileWriter(newMarketFile);

			// SCAN THE FILE AND CONVERT
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] dataByColumn = line.split(",");
				
				// IF IT IS NOT THE FIRST AND LAST, CONVERT IT
				if (!line.startsWith("Sec") && !line.startsWith("\n")){
					dataByColumn[2] = convertCurrencyColumn(dataByColumn[2], targetCurrency, currencyRate);
					dataByColumn[3] = convertCurrencyColumn(dataByColumn[3], targetCurrency, currencyRate);
					dataByColumn[4] = convertCurrencyColumn(dataByColumn[4], targetCurrency, currencyRate);
					dataByColumn[5] = convertCurrencyColumn(dataByColumn[5], targetCurrency, currencyRate);
					dataByColumn[7] = convertCurrencyColumn(dataByColumn[7], targetCurrency, currencyRate);
				}
				
				// WRITE THE LINE BACK TO WRITER
				String appendString = "";
				for (String column : dataByColumn){
					appendString += column + ",";
				}
				appendString = appendString.replaceFirst(".$","\n");
				writer.append(appendString);
				
			}
			writer.flush();
			writer.close();
			scanner.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// RETURN RESPONSE
		ConvertMarketDataResponse response = factory.createConvertMarketDataResponse();
		response.setEventSetId(newEventSetId);
		return response;
	}

	
	//========================================================================
	//							visualiseMarketData
	//========================================================================
	@Override
	public VisualiseMarketDataResponse visualiseMarketData(VisualiseMarketDataRequest parameters)throws VisualiseMarketDataFaultMsg {
		// GET PARAMETERS
		String eventSetId = parameters.getEventSetId();

		// FAULT HANDLING
		String faultTestFilePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".csv";
		File faultTestFile = new File(faultTestFilePath);
    	if (!faultTestFile.exists()){
    		String code = "Invalid eventSetId";
    		String msg = "There's no file with the given eventSetId";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new VisualiseMarketDataFaultMsg(msg, fault);
    	}
    	
    	// BEGIN SERVICE
		try {
			// GET MARKET DATA FILE
			String filePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".csv";
			File marketFile = new File(filePath);
			Scanner scanner = new Scanner(new FileReader(marketFile));
			
			// CHECK IF DIRECTORY EXISTS BEFORE CREATING A NEW FILE
	    	File dir = new File(System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/");
			if (!dir.exists()) 
				if(dir.mkdir())
					System.out.println("Dir created");
			
			// CREATE NEW MARKET FILE USING NEW EVENTSETID
			String htmlFilePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".html";
			File newHtmlFile = new File(htmlFilePath);
			FileWriter writer = new FileWriter(newHtmlFile);
			
			writer.append("<html><body><table border='1'");
			// SCAN THE FILE BY EACH LINE
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] dataByColumn = line.split(",");
				
				// WRITE IT TO WRITER
				writer.append("<tr>");
				for (String column : dataByColumn){
					writer.append("<td>");
					writer.append(column);
					writer.append("</td>");
				}
				writer.append("</tr>");
			}
			writer.append("</table></body></html>");
			writer.flush();
			writer.close();
			scanner.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String returnFilePath = String.format("http://localhost:8080/EventSetDownloads/%s.html", eventSetId);
		// RETURN RESPONSE
		VisualiseMarketDataResponse response = factory.createVisualiseMarketDataResponse();
		response.setDataURL(returnFilePath);
		return response;
	}

	
	
	//========================================================================
	//							HELPER FUNCTIONS
	//========================================================================

	public static String humanReadableByteCount(long bytes) {
		int unit = 1024;
		if (bytes < unit) {
			return bytes + " B";
		}
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		char prefix = ("KMGTPE").charAt(exp - 1);
		return String.format("%.1f %cB", bytes / Math.pow(unit, exp), prefix);
	}

	public Hashtable<String, String> getDataFromMarketFile(String eventSetId) {
		Hashtable<String, String> hashTable = new Hashtable<String, String>();

		// INITIALIZE ATTRIBUTES
		String sec = "";
		String startDate = "";
		String endDate = "";
		String currencyCode = "";
		String fileSize = "";
		
		try { 		
			// GET MARKET DATA FILE
			String filePath = System.getProperty("catalina.home") +"/webapps/ROOT/EventSetDownloads/" +eventSetId +".csv";
			File marketFile = new File(filePath);
			Scanner scanner = new Scanner(new FileReader(marketFile));

			// READ THE FILE AND SET ALL ATTRIBUTES
			int lineCounter = 1;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] dataByColumn = line.split(",");

				if (lineCounter == 2) { // IF IT IS THE SECOND LINE
					sec = dataByColumn[0];
					endDate = dataByColumn[1];
					currencyCode = dataByColumn[2].substring(0, 3);
				}
				
				if (!scanner.hasNextLine()) { // IF IT IS THE LAST LINE
					startDate = dataByColumn[1];
				}
				lineCounter++;
			}
			fileSize = humanReadableByteCount(marketFile.length());
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// PUT ALL ATTRIBUTES INTO HASH TABLE TO RETURN
		hashTable.put("eventSetId", eventSetId);
		hashTable.put("sec", sec);
		hashTable.put("startDate", startDate);
		hashTable.put("endDate", endDate);
		hashTable.put("currencyCode", currencyCode);
		hashTable.put("fileSize", fileSize);
		return hashTable;
	}

	public double getCurrencyRate(String targetCurrency, String targetDate) throws ConvertMarketDataFaultMsg {
		
		String uri = "http://www.xe.com/currencytables/?from=AUD&date="+ targetDate;
		try {
			// SET HTTP REQUEST
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("User-Agent", "Mozilla/5.0");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			// SEND REQUEST
			DataOutputStream httpConnectionWriter = new DataOutputStream(connection.getOutputStream());
			httpConnectionWriter.flush();
			httpConnectionWriter.close();

			// GET RESPONSE
			InputStream is = connection.getInputStream();
			BufferedReader httpResponseReader = new BufferedReader(new InputStreamReader(is));

			// PROCESS RESPONSE TO GET THE CURRENCY RATE
			String responseLine;
			while ((responseLine = httpResponseReader.readLine()) != null) {
				if (responseLine.matches(".*/currency/"+targetCurrency.toLowerCase()+".*")) {
					responseLine = responseLine.replaceAll("^.*(/currency/"+targetCurrency.toLowerCase()+")", "");
					responseLine = responseLine.replaceFirst("ICTRate\">", "-cuthere-");
					responseLine = responseLine.replaceFirst("^.*-cuthere-", "");
					responseLine = responseLine.replaceFirst("</td>.*$", "");
					break;
				}
			}
			httpResponseReader.close();
			return Double.parseDouble(responseLine);

		} catch (Exception e){
    		String code= "genericFault";
    		String msg = e.getClass().getSimpleName() +" - " + e.getMessage();
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ConvertMarketDataFaultMsg(msg, fault);
		}
	}
	
	private String convertCurrencyColumn(String column, String targetCurrency, double currencyRate) {
		// CONVERT THE STRING ACCORDING TO ARGUMENTS
		column = column.replaceFirst("^[A-Z]{3}", targetCurrency.toUpperCase());
		double value = Double.parseDouble(column.substring(3, column.length()));
		value = value * currencyRate;
		DecimalFormat df = new DecimalFormat("00.00");
		String newValue = df.format(value);
		column = column.replaceFirst("[0-9].*$", newValue);
		
		return column;
	}

}
