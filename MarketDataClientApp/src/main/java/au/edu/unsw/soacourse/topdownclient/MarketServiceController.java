package au.edu.unsw.soacourse.topdownclient;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import au.edu.unsw.soacourse.topdown.*;
import au.edu.unsw.soacourse.utilservice.*;



@Controller
public class MarketServiceController
{
	@Autowired
	private TopDownSimpleService simple; //this is instantiated by jaxws:client id=simple in dispatcher-servlet.xml
	@Autowired
	private MarketDataUtilService util;
	private LinkedList<String> eventSetIds = new LinkedList<String>();
	
	@RequestMapping(value="/topDownSimpleService")
	public String topDownSimpleService(ModelMap model, HttpServletRequest httpRequest)
	{
		if(httpRequest.getMethod().equalsIgnoreCase("post"))
		{
			String action = httpRequest.getParameter("btnSubmit");
			if(action.equalsIgnoreCase("import")) //importMarketData()
			{
				ImportMarketDataRequest request = new au.edu.unsw.soacourse.topdown.ObjectFactory().createImportMarketDataRequest();
				request.setSec(httpRequest.getParameter("sec"));
				request.setEndDate(String.format("%s-%s-%s",
						httpRequest.getParameter("endDay"),
						httpRequest.getParameter("endMonth"),
						httpRequest.getParameter("endYear")));
				request.setStartDate(String.format("%s-%s-%s",
						httpRequest.getParameter("startDay"),
						httpRequest.getParameter("startMonth"),
						httpRequest.getParameter("startYear")));
				
				try
				{
					ImportMarketDataResponse response = simple.importMarketData(request);
					model.addAttribute("importData", response.getEventSetId());
					model.addAttribute("importError", false);
					eventSetIds.add(response.getEventSetId());
				}
				catch (ImportMarketFaultMsg e)
				{
					model.addAttribute("importError", true);
					model.addAttribute("importErrorCode", e.getFaultInfo().getErrcode());
					model.addAttribute("importErrorText", e.getFaultInfo().getErrtext());
				}
			}
			else if(action.equalsIgnoreCase("download")) //downloadFile()
			{
				DownloadFileRequest request = new au.edu.unsw.soacourse.topdown.ObjectFactory().createDownloadFileRequest();
				request.setEventSetId(httpRequest.getParameter("eventSetId"));
				
				try
				{
					DownloadFileResponse response = simple.downloadFile(request);
					model.addAttribute("downloadData", response.getDataURL());
					model.addAttribute("downloadError", false);
				}
				catch (DownloadFileFaultMsg e)
				{
					model.addAttribute("downloadError", true);
					model.addAttribute("downloadErrorCode", e.getFaultInfo().getErrcode());
					model.addAttribute("downloadErrorText", e.getFaultInfo().getErrtext());
				}
			}
		}
		
		model.addAttribute("eventSetIds", eventSetIds);
		
		return "topDownSimpleService";
	}
	
	@RequestMapping("/marketDataUtilService")
	public String marketDataSimpleService(ModelMap model, HttpServletRequest httpRequest)
	{
		if(httpRequest.getMethod().equalsIgnoreCase("post")){
			String action = httpRequest.getParameter("action");
		
			if(action.equalsIgnoreCase("summariseData")){
				SummariseMarketDataRequest request = new au.edu.unsw.soacourse.utilservice.ObjectFactory().createSummariseMarketDataRequest();
				try{
					request.setEventSetId(httpRequest.getParameter("eventSetId"));
					
					SummariseMarketDataResponse response = util.summariseMarketData(request);
					model.addAttribute("eventSetId", response.getEventSetId());
					model.addAttribute("sec", response.getSec());
					model.addAttribute("startDate", response.getStartDate());
					model.addAttribute("endDate", response.getEndDate());
					model.addAttribute("currencyCode", response.getCurrencyCode());
					model.addAttribute("fileSize", response.getFileSize());
					model.addAttribute("summariseDataError", false);
				}
				catch (SummariseMarketDataFaultMsg e){
					model.addAttribute("summariseDataError", true);
					model.addAttribute("summariseDataErrorCode", e.getFaultInfo().getErrcode());
					model.addAttribute("summariseDataErrorText", e.getFaultInfo().getErrtext());
				}

			}
			else if(action.equalsIgnoreCase("convertData")){
				ConvertMarketDataRequest request = new au.edu.unsw.soacourse.utilservice.ObjectFactory().createConvertMarketDataRequest();
	
				request.setEventSetId(httpRequest.getParameter("eventSetId"));
				request.setTargetCurrency(httpRequest.getParameter("targetCurrency"));
				request.setTargetDate(httpRequest.getParameter("targetDate"));
				try{
					ConvertMarketDataResponse response = util.convertMarketData(request);
					model.addAttribute("eventSetId", response.getEventSetId());
					model.addAttribute("convertDataError", false);
				}
				catch (ConvertMarketDataFaultMsg e){
					model.addAttribute("convertDataError", true);
					model.addAttribute("convertDataErrorCode", e.getFaultInfo().getErrcode());
					model.addAttribute("convertDataErrorText", e.getFaultInfo().getErrtext());
				}
			}
			else if(action.equalsIgnoreCase("visualiseData")){
				VisualiseMarketDataRequest request = new au.edu.unsw.soacourse.utilservice.ObjectFactory().createVisualiseMarketDataRequest();
	
				request.setEventSetId(httpRequest.getParameter("eventSetId"));
				
				try{
					VisualiseMarketDataResponse response = util.visualiseMarketData(request);
					model.addAttribute("dataURL", response.getDataURL());
					model.addAttribute("visualiseDataError", false);
				}
				catch (VisualiseMarketDataFaultMsg e){
					model.addAttribute("visualiseDataError", true);
					model.addAttribute("visualiseDataErrorCode", e.getFaultInfo().getErrcode());
					model.addAttribute("visualiseDataErrorText", e.getFaultInfo().getErrtext());
				}
			}
		}
		
		model.addAttribute("eventSetIds", eventSetIds);
		
		return "marketDataUtilService";
	}
	   
}