<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<title>Market Data Client App - Market Data Util Service</title>
</head>
<body>
	<div class="navbar navbar-inverse">
		<div class="container">
			<ul class="nav navbar-nav">
				<li><a href="http://localhost:8080/MarketDataClientApp/topDownSimpleService">TopDown Simple Service</a></li>
				<li><a href="http://localhost:8080/MarketDataClientApp/marketDataUtilService">Market Data Util Service</a></li>
			</ul>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>Market Data Util Service</h3>
				<div class="jumbotron">
					<strong>List of your current Market Data (Event Set Ids):</strong>
					<ul>
						<c:forEach items="${eventSetIds}" var="item">
							<li><c:out value="${item}" /></li>
						</c:forEach>
					</ul>				
				</div>
				
			</div>
		</div>
		
		
		
		<table class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th width="50%">Market Data Summary</th>
					<th width="50%">Summarise Market Data Response</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form method="POST" class="form-horizontal">
						  <fieldset>
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">eventSetId</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="eventSetId" name="eventSetId">
						      </div>
						    </div>
						    
						    <div class="form-group">
						      <div class="col-lg-6 col-lg-offset-3">
						        <button type="submit" name="action" class="btn btn-primary" value="summariseData">Invoke Summarise Market Data</button>
						      </div>
						    </div>
						  </fieldset>
						</form>
					</td>
					<td>
						<c:choose>
							<c:when test="${summariseDataError eq 'true'}">
									The service has returned the following error:<br />
									Code: <c:out value="${summariseDataErrorCode}"></c:out>, <br />
									Text: <c:out value="${summariseDataErrorText}"></c:out>
							</c:when>
							<c:when test="${summariseDataError eq 'false'}">
								<table width="75%" align="center">
									<tr>
										<td><b>eventSetId</b></td>
										<td><c:out value="${eventSetId}"></c:out></td>
									</tr>
									<tr>
										<td><b>sec</b></td>
										<td><c:out value="${sec}"></c:out></td>
									</tr>
									<tr>
										<td><b>startDate</b></td>
										<td><c:out value="${startDate}"></c:out></td>
									</tr>
									<tr>
										<td><b>endDate</b></td>
										<td><c:out value="${endDate}"></c:out></td>
									</tr>
									<tr>
										<td><b>currencyCode</b></td>
										<td><c:out value="${currencyCode}"></c:out></td>
									</tr>
									<tr>
										<td><b>fileSize</b></td>
										<td><c:out value="${fileSize}"></c:out></td>
									</tr>
									
								</table>
						
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>
		
		<hr/>
		
		<table class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th width="50%">Currency Convert Market Data</th>
					<th width="50%">Convert Market Data Response</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form method="POST" class="form-horizontal">
						  <fieldset>
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">eventSetId</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="eventSetId" name="eventSetId">
						      </div>
						    </div>
					      	<div class="form-group">
						      <label class="col-lg-3 control-label">targetCurrency</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="targetCurrency" name="targetCurrency">
						      </div>
						    </div>
						    <div class="form-group">  
						      <label class="col-lg-3 control-label">targetDate</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="targetDate" name="targetDate">
						      </div>
						    </div>
					      	<div class="form-group">
						      <div class="col-lg-6 col-lg-offset-3">
						        <button type="submit" name="action" class="btn btn-primary" value="convertData">Invoke Convert Market Data</button>
						      </div>
						    </div>
						  </fieldset>
						</form>
					</td>
					<td>
						<c:choose>
							<c:when test="${convertDataError eq 'true'}">
								The service has returned the following error:<br />
								Code: <c:out value="${convertDataErrorCode}"></c:out>, <br />
								Text: <c:out value="${convertDataErrorText}"></c:out>
							</c:when>
							<c:when test="${convertDataError eq 'false'}">
								The service has convert the eventSetId into <br>
								<b><c:out value="${eventSetId}"></c:out></b> 
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>

		<hr />
		
		<table class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th width="50%">Visualise Market Data</th>
					<th width="50%">Visualise Market Data Response</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form method="POST" class="form-horizontal">
						  <fieldset>
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">eventSetId</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="eventSetId" name="eventSetId">
						      </div>
						    </div>
						    
						    <div class="form-group">
						      <div class="col-lg-6 col-lg-offset-3">
						        <button type="submit" name="action" class="btn btn-primary" value="visualiseData">Invoke Visualise Market Data</button>
						      </div>
						    </div>
						  </fieldset>
						</form>
					</td>
					<td>
						<c:choose>
							<c:when test="${visualiseDataError eq 'true'}">
									The service has returned the following error:<br />
									Code: <c:out value="${visualiseDataErrorCode}"></c:out>, <br />
									Text: <c:out value="${visualiseDataErrorText}"></c:out>
							</c:when>
							<c:when test="${visualiseDataError eq 'false'}">
									The produced HTML page: <br>
								<b><a href="<c:out value="${dataURL}"></c:out>"><c:out value="${dataURL}"></c:out></a></b>
								<br>
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>
		
		
		
		
		</div>
	



</body>
</html>







