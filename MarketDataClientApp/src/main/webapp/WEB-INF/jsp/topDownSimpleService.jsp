<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<title>Market Data Client App - Top Down Simple Service</title>
</head>
<body>
	<div class="navbar navbar-inverse">
		<div class="container">
			<ul class="nav navbar-nav">
				<li><a href="http://localhost:8080/MarketDataClientApp/topDownSimpleService">TopDown Simple Service</a></li>
				<li><a href="http://localhost:8080/MarketDataClientApp/marketDataUtilService">Market Data Util Service</a></li>
			</ul>
		</div>
	</div>
	
	
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>TopDown Simple Service</h3>
				<div class="jumbotron">
					<strong>List of your current Market Data (Event Set Ids):</strong>
					<ul>
						<c:forEach items="${eventSetIds}" var="item">
							<li><c:out value="${item}" /></li>
						</c:forEach>
					</ul>				
				</div>
				
			</div>
		</div>
		
		
		
		<table class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th width="50%">Import Market Data</th>
					<th width="50%">Import Market Data Response</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form method="POST" class="form-horizontal">
						  <fieldset>
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">Stock Code</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="sec" name="sec">
						      </div>
						    </div>
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">Start Date</label>
						      <div class="col-lg-2">
							        Day
							        <select name="startDay" class="form-control">
									    <option value="01">01</option>
									    <option value="02">02</option>
									    <option value="03">03</option>
									    <option value="04">04</option>
									    <option value="05">05</option>
									    <option value="06">06</option>
									    <option value="07">07</option>
									    <option value="08">08</option>
									    <option value="09">09</option>
									    <option value="10">10</option>
									    <option value="11">11</option>
									    <option value="12">12</option>
									    <option value="13">13</option>
									    <option value="14">14</option>
									    <option value="15">15</option>
									    <option value="16">16</option>
									    <option value="17">17</option>
									    <option value="18">18</option>
									    <option value="19">19</option>
									    <option value="20">20</option>
									    <option value="21">21</option>
									    <option value="22">22</option>
									    <option value="23">23</option>
									    <option value="24">24</option>
									    <option value="25">25</option>
									    <option value="26">26</option>
									    <option value="27">27</option>
									    <option value="28">28</option>
									    <option value="29">29</option>
									    <option value="30">30</option>
									    <option value="31">31</option>
									</select>
								</div>
								<div class="col-lg-3">
									Month
									<select name="startMonth" class="form-control">
										<option selected value="01">Jan</option>
										<option value="02">Feb</option>
										<option value="03">Mar</option>
										<option value="04">Apr</option>
										<option value="05">May</option>
										<option value="06">Jun</option>
										<option value="07">Jul</option>
										<option value="08">Aug</option>
										<option value="09">Sep</option>
										<option value="10">Oct</option>
										<option value="11">Nov</option>
										<option value="12">Dec</option>
									</select>
								</div>
								<div class="col-lg-3">
									Year <input type="text" class="form-control" name="startYear" size="12" placeholder="year"/><br/>
								</div>
						      </div>
						    </div>
						    
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">End Date</label>
						      <div class="col-lg-2">
						        	Day
									<select name="endDay" class="form-control">
									    <option value="01">01</option>
									    <option value="02">02</option>
									    <option value="03">03</option>
									    <option value="04">04</option>
									    <option value="05">05</option>
									    <option value="06">06</option>
									    <option value="07">07</option>
									    <option value="08">08</option>
									    <option value="09">09</option>
									    <option value="10">10</option>
									    <option value="11">11</option>
									    <option value="12">12</option>
									    <option value="13">13</option>
									    <option value="14">14</option>
									    <option value="15">15</option>
									    <option value="16">16</option>
									    <option value="17">17</option>
									    <option value="18">18</option>
									    <option value="19">19</option>
									    <option value="20">20</option>
									    <option value="21">21</option>
									    <option value="22">22</option>
									    <option value="23">23</option>
									    <option value="24">24</option>
									    <option value="25">25</option>
									    <option value="26">26</option>
									    <option value="27">27</option>
									    <option value="28">28</option>
									    <option value="29">29</option>
									    <option value="30">30</option>
									    <option value="31">31</option>
									</select>
								</div>
								<div class="col-lg-3">
									Month
									<select name="endMonth" class="form-control">
										<option selected value="01">Jan</option>
										<option value="02">Feb</option>
										<option value="03">Mar</option>
										<option value="04">Apr</option>
										<option value="05">May</option>
										<option value="06">Jun</option>
										<option value="07">Jul</option>
										<option value="08">Aug</option>
										<option value="09">Sep</option>
										<option value="10">Oct</option>
										<option value="11">Nov</option>
										<option value="12">Dec</option>
									</select>
								</div>
								<div class="col-lg-3">
									Year <input type="text" class="form-control" name="endYear" size="12" placeholder="year"/><br/>
								</div>
						      </div>
						    </div>
						    
						    <div class="form-group">
						      <div class="col-lg-6 col-lg-offset-3">
						        <button type="submit" name="btnSubmit" class="btn btn-primary" value="import">Invoke Import Market Data</button>
						      </div>
						    </div>
						  </fieldset>
						</form>
					</td>
					<td>
						<c:choose>
							<c:when test="${importError eq 'true'}">
								The service has returned the following error:<br/>
								Code: <c:out value="${importErrorCode}"></c:out>, <br/>
								Text: <c:out value="${importErrorText}"></c:out>
							</c:when>
							<c:when test="${importError eq 'false'}">
							New Event Set Id from the service:  <br>
								<b><c:out value="${importData}"></c:out></b> 
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>
		
		<hr/>
		
		<table class="table table-bordered" width="100%">
			<thead>
				<tr>
					<th width="50%">Download Market Data</th>
					<th width="50%">Download Market Data Response</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<form method="POST" class="form-horizontal">
						  <fieldset>
						    
						    <div class="form-group">
						      <label class="col-lg-3 control-label">Event Set Id:</label>
						      <div class="col-lg-6">
						        <input type="text" class="form-control" placeholder="eventSetId" name="eventSetId">
						      </div>
						    </div>
					      	
					      	<div class="form-group">
						      <div class="col-lg-6 col-lg-offset-3">
						        <button type="submit" name="btnSubmit" class="btn btn-primary" value="download">Invoke Download Market Data</button>
						      </div>
						    </div>
						  </fieldset>
						</form>
					</td>
					<td>
						<c:choose>
							<c:when test="${downloadError eq 'true'}">
								The service has returned the following error:<br/>
								Code: <c:out value="${downloadErrorCode}"></c:out>, <br/>
								Text: <c:out value="${downloadErrorText}"></c:out>
							</c:when>
							<c:when test="${downloadError eq 'false'}">
								The Market Data file is ready for download at: <br>
								<a href=<c:out value="${downloadData}"></c:out>><c:out value="${downloadData}"></c:out></a>
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</tbody>
		</table>


		
		
		
		
		</div>
	
	

	
</body>
</html>