<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<title>Market Data Client App - Index</title>
</head>
<body>
	<div class="navbar navbar-inverse">
		<div class="container">
			<ul class="nav navbar-nav">
				<li><a href="http://localhost:8080/MarketDataClientApp/topDownSimpleService">TopDown Simple Service</a></li>
				<li><a href="http://localhost:8080/MarketDataClientApp/marketDataUtilService">Market Data Util Service</a></li>
			</ul>
		</div>
	</div>




	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>Market Data Client App Index</h3>			
			</div>
		</div>
		<a href="http://localhost:8080/MarketDataClientApp/topDownSimpleService">Go to Top Down Simple Service></a> <br/><br/>
		<a href="http://localhost:8080/MarketDataClientApp/marketDataUtilService">Go to Market Data Util Service></a>
	</div>
		
	
	
	
</body>
</html>