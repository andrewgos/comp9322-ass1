package au.edu.unsw.soacourse.topdown;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.jws.WebService;


@WebService(endpointInterface="au.edu.unsw.soacourse.topdown.TopDownSimpleService")
public class TopDownSimpleServiceImpl implements TopDownSimpleService
{
	private final static String FILE_EXT = ".csv";
	private final static String SERVER_PATH = System.getProperty("catalina.home");
	private final static String APP_PATH = SERVER_PATH + "/webapps/ROOT/EventSetDownloads/";
	
	private ObjectFactory factory = new ObjectFactory();
	
    public ImportMarketDataResponse importMarketData(ImportMarketDataRequest parameters) throws ImportMarketFaultMsg
    {
    	String sec, dataSourceUrl, start, end;
    	Calendar endDate = Calendar.getInstance();
    	Calendar startDate = Calendar.getInstance();
    	SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
    	
    	end = parameters.getEndDate();
    	start = parameters.getStartDate();
    	sec =  parameters.getSec().toUpperCase();
    	dataSourceUrl = parameters.getDataSourceURL();
    	
    	// Check for valid SEC code format
    	if(!sec.matches("[A-Z]{3}"))
    	{
    		String code = "Invalid SEC code";
    		String msg = "SEC code must only be 3 letters";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ImportMarketFaultMsg(msg, fault);
    	}
    	
    	// Check for valid Date format
    	try
    	{
    		df.parse(end);
    		df.parse(start);
    	}
    	catch(ParseException e)
    	{
    		String code = "Invalid Date format";
    		String msg = "Date must be in the format: dd-MM-yyyy";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ImportMarketFaultMsg(msg, fault);
    	}
    	
    	String[] endDateFields = end.split("-");
    	String[] startDateFields = start.split("-");
    	
    	int endDateDay = Integer.parseInt(endDateFields[0]);
    	int endDateMonth = Integer.parseInt(endDateFields[1]);
    	int endDateYear = Integer.parseInt(endDateFields[2]);
    	
    	int startDateDay = Integer.parseInt(startDateFields[0]);
    	int startDateMonth = Integer.parseInt(startDateFields[1]);
    	int startDateYear = Integer.parseInt(startDateFields[2]);
    	
    	// Check for invalid day
    	if (startDateDay < 1 || endDateDay < 1 || startDateDay > 31 || endDateDay > 31)
    	{
    		String code= "Invalid Date";
    		String msg = "Day must be between 01 and 31";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ImportMarketFaultMsg(msg, fault);
    	}
    	
    	// Check for invalid month
    	if (startDateMonth < 1 || endDateMonth < 1 || startDateMonth > 12 || endDateMonth > 12)
    	{
    		String code= "Invalid Date";
    		String msg = "Month must be between 01 and 12";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ImportMarketFaultMsg(msg, fault);
    	}

    	endDate.set(Calendar.DATE, endDateDay);
    	endDate.set(Calendar.YEAR, endDateYear);
    	endDate.set(Calendar.MONTH, endDateMonth - 1);
    	
    	startDate.set(Calendar.DATE, startDateDay);
    	startDate.set(Calendar.YEAR, startDateYear);
    	startDate.set(Calendar.MONTH, startDateMonth - 1);
    	
    	// Check for start date must be before end date
    	if (endDate.getTimeInMillis() < startDate.getTimeInMillis())
    	{
    		String code= "Invalid Date";
    		String msg = "End Date must not be before Start Date";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ImportMarketFaultMsg(msg, fault);
    	}
    	
    	String eventSetId = getMarketData(sec, startDate, endDate, dataSourceUrl);
    	ImportMarketDataResponse response = factory.createImportMarketDataResponse();
    	response.setEventSetId(eventSetId);
    	
    	return response;
    }

    public DownloadFileResponse downloadFile(DownloadFileRequest parameters) throws DownloadFileFaultMsg
    {
    	File file = new File (APP_PATH + parameters.getEventSetId() + FILE_EXT);
    	
    	if (!file.exists())
    	{
    		String code = "Invalid eventSetId";
    		String msg = "There's no file with the given eventSetId";
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new DownloadFileFaultMsg(msg, fault);
    	}
    	
    	String uri = String.format("http://localhost:8080/EventSetDownloads/%s.csv", parameters.getEventSetId());
    	DownloadFileResponse response = factory.createDownloadFileResponse();
    	response.setDataURL(uri);
    	
    	return response;
    }
    
    private String getMarketData(String stockCode, Calendar from, Calendar to, String dataSourceUrl) throws ImportMarketFaultMsg
    {
    	int lineNum;
    	String line;
    	String eventSetId = UUID.randomUUID().toString();
    	String uri = String.format("http://real-chart.finance.yahoo.com/table.csv?s=%s.AX&a=%s&b=%s&c=%s&d=%s&e=%s&f=%s&g=d&ignore=.csv", 
    			stockCode, from.get(Calendar.MONTH), from.get(Calendar.DATE), from.get(Calendar.YEAR),
    			to.get(Calendar.MONTH), to.get(Calendar.DATE), to.get(Calendar.YEAR));
    	
    	try
    	{
    		File directory = new File(APP_PATH);
    		if (!directory.exists()) directory.mkdir();
    		
			URL url = new URL(uri);
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			File file = new File(APP_PATH + eventSetId + FILE_EXT);
			FileWriter writer = new FileWriter(file);

			lineNum = 0;
			while((line = reader.readLine()) != null)
			{
				if(lineNum == 0) // Header Row
				{
					writer.append("Sec," + line);
				}
				else // Data Rows
				{
					writer.append(stockCode + ",");
					String[] column = line.split(",");
					
					for (int i = 0; i < column.length; i++)
					{
						// All the price columns
						if (i >= 1 && i <= 4 || i == 6)
							writer.append("AUD" + column[i]);
						else
							writer.append(column[i]);
						
						// Except the last column
						if (i < column.length - 1)
							writer.append(',');
					}
				}
				
				writer.append('\n');
				lineNum++;
			}
			
			writer.flush();
			writer.close();
			reader.close();
		}
    	catch (Exception e)
    	{
    		String code= "genericFault";
    		String msg = e.getClass().getSimpleName() +" - " + e.getMessage();
    		
    		ServiceFaultType fault = factory.createServiceFaultType();
    		fault.setErrcode(code);
    		fault.setErrtext(msg);
    		
    		throw new ImportMarketFaultMsg(msg, fault);
		}
    	
    	return eventSetId;
    }

}
